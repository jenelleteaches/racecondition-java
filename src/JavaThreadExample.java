
class JavaThreadExample extends Thread {
	
	public static void main(String[] args) throws InterruptedException {
        System.out.println("Starting the PSVM main thread");
        Thread.sleep(1000);
        
        // start thread 1
        Thread t1 = new CustomThread("thread-01");
        t1.start();
        
        // SCENARIO 1: PVSM Wait forever until T1 finishes
        System.out.println("PSVM is waiting for thread 1 to finish");
        System.out.println("    PSVM will wait for t1, even if t1 takes forever");
        t1.join();
        System.out.println("Thread 1 finished.");
        
        // start thread 2
        System.out.println("Thread 2 is about to start...");
        Thread t2 = new CustomThread("thread-02");
        t2.start();
        
        // SCENARIO 2: PVSM will only wait 1 second before it keeps moving on 
        System.out.println("PSVM thread will wait 1 seconds for t2 to finish");
        System.out.println("   If t2 does not finish in 1 second, PVSM will continue");
        t2.join(1000);
        
        // end of program!
        System.out.println("T2 did not finish in 1 seconds, so moving on");
        System.out.println("PVSM reached end of program...");
    }
	    
}

class CustomThread extends Thread {
	public CustomThread(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " sub thread is starting");
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + " sub thread finished");
    } 
}
