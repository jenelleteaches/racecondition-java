
public class ThreadExample02 {

	public static void main(String[] args) {
		Thread t1 = new Thread(new JenelleThread("e1"));
		t1.start();
		Thread t2 = new Thread(new JenelleThread("e2"));
		t2.start();

		while (true) {
			System.out.println("PVSM is in the loop");
		    try {
		        t1.join(); // 1
		        t2.join(); // 2  These lines (1,2) are in in public static void main
		        //break;
		    }
		    catch(Exception e) {
		    	e.printStackTrace();
		    }
		}

	}

}

class JenelleThread extends Thread implements Runnable {
	public JenelleThread(String threadName) {
		super(threadName);
	}

	public void run(){
		System.out.println("MyRunnable running");
	}
}
