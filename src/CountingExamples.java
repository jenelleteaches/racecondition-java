// Inner class that represents a counter
class Counter {
	int counter = 0;
	
	public Counter() {
		// empty constructor
	}
	
	// accessor methods
	public void increment() {
		this.counter = this.counter + 1;
	}
	public int getCount() {
		return counter;
	}
}

//Thread that uses the counter
class ThreadCounter extends Thread {
	private Counter counterObject;
	
	public ThreadCounter(Counter c) {
		this.counterObject = c;
	}
	public void run() {
		for (int i = 0; i < 500; i++) {
			this.counterObject.increment();
		}
	}
}
		
class CountingExamples {
	public static void main(String[] args) throws Exception {
		
		// In this context, the FINAL keyword signals to the compiler
		// 	that it is safe for threads to read the counterObject
		// Also, you won't end up in a weird state where one thread
		// 	attempts to read the counterObject while the other thread
		// 	is in process of creating the object
		final Counter counterObject = new Counter();
		
		ThreadCounter thread1 = new ThreadCounter(counterObject);
		ThreadCounter thread2 = new ThreadCounter(counterObject);
		
		// start the threads
		thread1.start();
		thread2.start();
		
		// join the threads
		thread1.join();
		thread2.join();
		
		// output the values in the counter
		System.out.println(counterObject.getCount());
		
	}
}
